import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import VueRouter from 'vue-router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import axios from "axios";

import MainPage from './components/MainPage.vue'
import CategoryPage from './components/CategoryPage.vue'
import Product from './components/Product.vue'
import AboutPage from './components/AboutPage.vue'
import CartPage from './components/CartPage.vue'
import { create } from 'domain'

Vue.use(BootstrapVue)
Vue.use(VueRouter)

Vue.config.productionTip = false

const router = new VueRouter({
  routes: [
    { path: '/', component: MainPage },
    { path: '/about-us', component: AboutPage },
    { path: '/categories/:categoryAlias', component: CategoryPage },
    { path: '/products/:productId', component: Product },
    {path: '/cart', component: CartPage}
  ],
  mode: 'history'
});


axios.defaults.headers.common['Authorization']
= 'Bearer markbossardo@gmail.com';
if(localStorage.cartId){
  axios.get("https://euas.person.ee/user/carts/" +localStorage.cartId)
  .then(response => {
    

    new Vue({
      render: h => h(App),
      router: router,
      data:{
        cart: response.data,
        saveCart(){
          axios.put("https://euas.person.ee/user/carts/"+this.cart.id, this.cart)
      }
      }
    }).$mount('#app');
  })
} else{
  axios.post("https://euas.person.ee/user/carts")
    .then(response => {
      localStorage.cartId=response.data.id
      new Vue({
        render: h => h(App),
        router: router,
        data:{
          cart: response.data,
          saveCart(){
              axios.put("https://euas.person.ee/user/carts/"+this.cart.id, this.cart)
          }
        }
      }).$mount('#app');
    })
  }
/*new Vue({
  render: h => h(App),
  router: router,
}).$mount('#app'); */
 




